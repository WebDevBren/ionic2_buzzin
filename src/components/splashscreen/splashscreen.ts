import { Component } from '@angular/core';

/*
  Generated class for the Splashscreen component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'splashscreen',
  templateUrl: 'splashscreen.html'
})
export class SplashscreenComponent {

  images: string[] = [
    "https://some.img.site"
  ];

  safetyText: string[] = [
    "please don't play while driving",
  ];

  getRandomMessage() {
    return this.safetyText[Math.floor(Math.random()*this.safetyText.length)];
  }

  getRandomImage() {
    this.images[Math.floor(Math.random()*this.images.length)];
  }


  constructor() {
  }

}
