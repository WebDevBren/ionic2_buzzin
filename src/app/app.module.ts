import { NgModule, ErrorHandler } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Storage } from '@ionic/storage';
//Custom components
import { CollectionItem, CollectionItemInfo } from './CollectionItem';
//Custom Directives
//Pages
import { HomePage } from '../pages/home/home';
import { MapPage } from '../pages/map/map';
import { LoginPage } from '../pages/login/login';
import { ResetPasswordPage } from '../pages/resetPassword/reset-password';
import { RegistrationPage } from '../pages/registration/registration';
import { CollectionsPage } from '../pages/collections/collections';
import { AboutPage } from '../pages/about/about';
import { LicensePage } from '../pages/license/license';

//Importing Services
import { ConnectivityService } from '../services/connectivity-service';
import { WikitudeService } from '../services/wikitude-service';
import { MapService } from '../services/map-service';
import { LocationService } from '../services/location-service';
import { EverliveService } from '../services/everlive-service';
import { UserService } from '../services/user-service';

import 'hammerjs';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MapPage,
    LoginPage,
    ResetPasswordPage,
    RegistrationPage,
    CollectionsPage,
    CollectionItem,
    CollectionItemInfo,
    AboutPage,
    LicensePage
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      mode: 'md',
      statusbarPadding: false
    }),
    MaterialModule.forRoot(),
    FormsModule,
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MapPage,
    LoginPage,
    ResetPasswordPage,
    RegistrationPage,
    CollectionsPage,
    ///////////////
    CollectionItem,
    CollectionItemInfo,
    AboutPage,
    LicensePage
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ConnectivityService,
    WikitudeService,
    MapService,
    LocationService,
    EverliveService,
    UserService,
    Storage,
  ]
})
export class AppModule { }
