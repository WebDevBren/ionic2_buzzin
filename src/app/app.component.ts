import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { UserService } from '../services/user-service';

import { EverliveService } from '../services/everlive-service';

//pages

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { CollectionsPage } from '../pages/collections/collections';
import { AboutPage } from '../pages/about/about';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage = HomePage;

  @ViewChild('content') nav: Nav;

  pageTitle = "BUZZIN";

  firstName: string = "";

  unauthenticatedMenu: any[] = [
    {
      icon: 'md-home',
      text: 'Home',
      action: () => { this.nav.setRoot(HomePage); this.closeMenu(); }
    },
    {
      icon: 'ios-information-circle-outline',
      text: 'About',
      action: () => { this.nav.push(AboutPage); this.closeMenu(); }
    },
    {
      icon: 'md-lock',
      text: 'Login',
      action: () => { this.nav.setRoot(LoginPage); this.closeMenu(); }
    },

  ]

  authenticatedMenu: any[] = [
    {
      icon: 'md-home',
      text: 'Home',
      action: () => { this.nav.setRoot(HomePage); this.closeMenu(); }
    },
    {
      icon: 'ios-information-circle-outline',
      text: 'About',
      action: () => { this.nav.push(AboutPage); this.closeMenu(); }
    },
    {
      icon: 'ios-keypad',
      text: 'Collections',
      action: () => { this.nav.push(CollectionsPage) }
    },
    {
      icon: 'md-unlock',
      text: 'Log out',
      action: () => {
        this.user.logout();
        this.nav.setRoot(LoginPage);
      }
    }
  ]

  extraButtons: any[] = [];

  constructor(platform: Platform, everlive: EverliveService, public menu: MenuController, public user: UserService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.backgroundColorByHexString('#1D9DCD');
      Splashscreen.hide();

      everlive.APP_ID = "lacs5sag3ylbwzm7";
      everlive.MASTER_KEY = "YqRLXyN9xZCCo5IgYisa4Uh5Myjfh540";

      this.user.restore();
    });
  }


  openMenu() {
    this.menu.open('nav');
  }

  closeMenu() {
    this.menu.close('nav');
  }

  toggleMenu() {

    console.log('attempting to toggle menu')

    if (this.menu.isOpen) {
      this.closeMenu();
    } else {
      this.openMenu();
    }
  }
}

