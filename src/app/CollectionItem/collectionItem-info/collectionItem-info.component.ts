import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
@Component({
    selector: 'collection-item-info',
    templateUrl: './collectionItem-info.component.html',
})
export class CollectionItemInfo {

    content: {
        title?: string,
        description?: string
    } = {};

    constructor(params: NavParams, public viewCtrl: ViewController) {
        this.content.description = params.get('description');
        this.content.title = params.get('title');
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }


}