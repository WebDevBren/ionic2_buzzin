import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CollectionItemInfo } from './collectionItem-info/collectionItem-info.component';


@Component({
    selector: 'collection-item',
    templateUrl: './collectionItem.component.html'
})
export class CollectionItem {

    @Input('collectionItem') collection: any = {};

    constructor() { }

    // moreInfo(collectionItem) {
    //     this.infoModal.data = { title: collectionItem.Name, description: collectionItem.Info };
    //     this.infoModal.present();
    // }

    onItemClick(collectionItem) {
        this.onIndividualClick.emit({ value: collectionItem });
    }

    prizeButton() {
        this.onButtonClick.emit({ value: this.collection });
    }

    @Output('item-click') onIndividualClick: EventEmitter<any> = new EventEmitter<any>();

    @Output('button-click') onButtonClick: EventEmitter<any> = new EventEmitter<any>();
}