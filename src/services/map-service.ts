import { Injectable, ElementRef } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { GoogleMap, GoogleMapsEvent, GoogleMapsLatLng } from 'ionic-native';

@Injectable()
export class MapService {

    public map: GoogleMap;

    public isInit: boolean = false;

    private _mapInit: Subject<any> = new Subject<any>();

    public MapInit: Observable<any> = this._mapInit.asObservable();

    private _isAvailable: Subject<boolean> = new Subject<boolean>();

    public MapAvailable: Observable<boolean> = this._isAvailable.asObservable();

    checkAvailable(): void {
        plugin.google.maps.Map.isAvailable((isAvailable: boolean, message: null | string) => {
            if (isAvailable) {
                this._isAvailable.next(isAvailable);
            } else {
                if (message) {
                    this._isAvailable.error(message);
                } else {
                    this._isAvailable.error('Unknown Error');
                }

            }
        });
    }

    createMap(DivElement: ElementRef) {
        this.map = new GoogleMap(DivElement.nativeElement, {
            'backgroundColor': 'white',
            'controls': {
                'compass': true,
                'myLocationButton': false,
                'indoorPicker': false,
                'zoom': true
            },
            'gestures': {
                'scroll': true,
                'tilt': true,
                'rotate': true,
                'zoom': true
            }
        });
        this.map.on(GoogleMapsEvent.MAP_READY).subscribe(
            () => {
                this.isInit = true;
                if (this.map) {
                    this._mapInit.next(this.map);
                }
            },
            (error) => { console.log('error', error) }
        );
    }

    createMarker(markerOptions: any) {
        return this.map.addMarker(markerOptions);
    }

    createCircle(circleOptions: any) {
        return this.map.addCircle(circleOptions);
    }

    createLatLng(latitude: number, longitude: number): GoogleMapsLatLng {
        return new GoogleMapsLatLng(latitude, longitude);
    }

    setCenter(latLng: GoogleMapsLatLng) {
        return this.map.setCenter(latLng);
    }

}