import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class LocationService {

    private defaultSettings: PositionOptions = { enableHighAccuracy: true, timeout: 10000, maximumAge: 3000 };

    private _currentLocation: Subject<Position> = new Subject<Position>();

    public currentLocation: Observable<Position> = this._currentLocation.asObservable();

    private _watcher;

    getCurrentLocation(settings = this.defaultSettings) {
        window.navigator.geolocation.getCurrentPosition((pos) => {
            if (pos) {
                this._currentLocation.next(pos);
            }
        }, (error: PositionError) => {
            if (error) {
                this._currentLocation.error(error);
            }
        },
            settings
        );

        return this.currentLocation;
    }

    watchLocation(settings = this.defaultSettings) {
        this._watcher = window.navigator.geolocation.watchPosition(
            (pos) => {
                if (pos) {
                    this._currentLocation.next(pos);
                }
            },
            (error) => {
                if (error) {
                    this._currentLocation.error(error);
                }
            },
            settings
        );
         return this.currentLocation;
    }

    stopWatchingLocation() {
        window.navigator.geolocation.clearWatch(this._watcher);
        this._currentLocation.complete();
    }

}

export interface geolocation {

}