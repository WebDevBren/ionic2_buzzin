import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

import { Storage } from '@ionic/storage';

import { EverliveService } from './everlive-service';

@Injectable()
export class UserService {

    private _auth_token: string = "";
    private _user_id: string = "";

    public isLoggedIn: boolean = false;

    public states = { 'LOGGED_IN': 'LOGGED_IN', 'LOGGED_OUT': 'LOGGED_OUT', 'ERROR': 'ERROR', 'REGISTERED': 'REGISTERED' };

    private authChange: Subject<any> = new Subject<any>();

    public AuthState: Observable<any> = this.authChange.asObservable();

    public details: EverliveUser = { Id: '', DisplayName: '', Username: '', ShortName: '', Email: '' };

    constructor(public everlive: EverliveService, public storage: Storage) { }

    login(Username: string, Password: string, rememberMe: boolean = false) {
        var userDetails = { "email": Username, "password": Password, "grant_type": "password" };

        this.everlive
            .createData('oauth/token', userDetails)
            .subscribe(
            (data) => {

                if (data) {
                    this._auth_token = data.Result.access_token;
                    this._user_id = data.Result.principal_id;

                    this.authChange.next({ state: 'LOGGED_IN', timestamp: new Date() });

                    // this._hasLoggedIn.next(true);
                    this.everlive.AUTH_KEY = data.Result.access_token;
                    this.isLoggedIn = true;

                    if (rememberMe) {
                        this.rememberMe(data.Result.access_token);
                    }

                    this.me();
                }
            },
            (error) => {
                console.warn(error.message)
                this.authChange.error({ state: 'ERROR', detail: error , timestamp: new Date() });
                // this._hasLoggedIn.error(error.message);
            }
            );

    }

    logout() {
        this.everlive.readData('oauth/logout', {
            "Authorization": `Bearer ${this._auth_token}`
        }).subscribe(
            () => {
                // this._hasLoggedOut.next(true);
                this.authChange.next({ state: 'LOGGED_OUT', timestamp: new Date() });

                this.isLoggedIn = false;

                this.clearCached();
            },
            (e) => {
                this.authChange.error({ state: 'ERROR', detail: e, timestamp: new Date });
            }
            )
    }

    resetPassword(Email: string) {
        return this.everlive.createData('Users/resetPassword', {
            "Email": Email
        });
    }

    register(username, displayName, email, password) {
        return this.everlive.createData('Users', {
            "Username": username,
            "Password": password,
            "Email": email,
            "DisplayName": displayName,
            "ShortName": displayName.substr(0, displayName.indexOf(' '))
        });
    }

    /** Currently only supports facebook */
    loginOrRegisterWithProvider(providerName: string, providerToken: string) {
        let user = {
            "Identity": {
                "Provider": providerName,
                "Token": providerToken
            }
        };

        return this.everlive
            .createData('Users', user)
            .subscribe(
            (data) => {

                console.log(data);

                this._auth_token = data.Result.access_token;
                // this._hasLoggedIn.next(true);
                this.authChange.next({ state: 'LOGGED_IN', timestamp: new Date() });
                this.everlive.AUTH_KEY = data.Result.access_token;
                this.isLoggedIn = true;

                this.me();
            }
            );
    }

    me() {
        if (!this.isLoggedIn) return;

        return this.everlive.readData('Users/me', {
            'Authorization': `Bearer ${this._auth_token}`
        }).subscribe(data => {
            this.details = data.Result
            let dname = this.details.DisplayName;
            this.details.ShortName = dname.substr(0, dname.indexOf(' '))
        }, error => this.isLoggedIn = false);
    }

    updateAcount(updateDetails: any) {
        if (!this.isLoggedIn) return;

        let currentUser = this.details;

        let updatedUser = Object.assign(currentUser, updateDetails);

        return this.everlive.updateData('User', updatedUser, {
            'Authorization': `Bearer ${this._auth_token}`
        });
    }

    rememberMe(authToken: string) {
        authToken = authToken || this._auth_token;
        console.log('attempting to store', authToken);
        this.storage.set('auth_key', authToken);
    }

    restore() {
        console.log('attempting to restore');
        this.storage.get('auth_key').then((value) => {
            console.log(value);
            if (value) {
                this._auth_token = value;
                this.isLoggedIn = true;

                this.me();
            }
        });
    }

    clearCached() {
        this.storage.set('auth_key', null);
    }

}


export interface EverliveUser {
    Id: string,
    CreatedAt?: Date,
    CreatedBy?: string,
    DisplayName: string,
    ShortName: string,
    Email: string,
    Username: string
};