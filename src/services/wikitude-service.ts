import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class WikitudeService {

    private _deviceSupport: Subject<any> = new Subject<string>();
    private _experienceLoad: Subject<any> = new Subject<string>();

    public deviceSupport: Observable<any> = this._deviceSupport.asObservable();
    public experienceLoad: Observable<any> = this._experienceLoad.asObservable();

    public AR: any = cordova.require("com.wikitude.phonegap.WikitudePlugin.WikitudePlugin");

    requiredFeatures: string[] = ["2d_tracking", "geo"];
    startupConfiguration: any = {};

    private _worldExit: Subject<any> = new Subject();
    public WorldExit: Observable<string> = this._worldExit.asObservable();

    constructor() {
        this.AR.setOnUrlInvokeCallback((url) => {
            if (url.indexOf('closeWikitudePlugin') !== -1) {
                this.AR.close();
                this._worldExit.next(true);
            } else {
                this.AR.hide();
            }
        });
    }

    checkDeviceCompatability() {
        this.AR.isDeviceSupported(
            (supported) => {
                this._deviceSupport.next(supported)
            }, () => {
                this._deviceSupport.error('not supported');
            }, this.requiredFeatures);
    }

    /** loads the Cordova Plugin */
    loadWorld(WorldUrl) {
        this.AR.loadARchitectWorld(
            (url) => { this._experienceLoad.next(url) },
            (error) => { this._experienceLoad.error(error) },
            WorldUrl,
            this.requiredFeatures,
            this.startupConfiguration
        );
    }

}