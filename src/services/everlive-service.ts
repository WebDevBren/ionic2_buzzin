import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';


@Injectable()
export class EverliveService {

    private _everlive_url = "https://api.everlive.com/v1";
    private _app_id: string = null;
    private _master_token = null;
    private _auth_token: string = null;

    get AuthToken() {
        if (this._auth_token) {
            return this._master_token;
        } else {
            return this._auth_token;
        }
    }

    set APP_ID(appId: string) {
        this._app_id = appId;
    }

    set EVERLIVE_URL(newUrl: string) {
        this._everlive_url = newUrl;
    }

    set MASTER_KEY(masterKey: string) {
        this._master_token = masterKey;
    }

    set AUTH_KEY(authkey: string) {
        this._auth_token = `bearer ${authkey}`;
    }

    get APP_URL(): string {
        return [this._everlive_url, this._app_id].join('/');
    }

    private defaultHeaders: any = { 'Content-Type': 'application/json' };

    constructor(public http: Http) { }


    readData(tableName: string, customHeaders?: any) {
        customHeaders = customHeaders || {};

        let headerObject = {};

        Object.assign(headerObject ,this.defaultHeaders, customHeaders);

        var headers = new Headers(headerObject);

        let Url: string = [this._everlive_url, this._app_id, tableName].join('/');

        console.log('what gets sent', headers);

        return this.http.get(Url, {
            headers: headers
        }).map((res: Response) => res.json())

    }

    createData(tableName: string, newDataContent: any, customHeaders?: any) {
        customHeaders = customHeaders || {};

        let headerObject = {};

        Object.assign(headerObject ,this.defaultHeaders, customHeaders);
        var headers = new Headers(headerObject);

        let Url: string = [this._everlive_url, this._app_id, tableName].join('/');

        return this.http.post(Url, newDataContent, {
            headers: headers
        }).map((res: Response) => res.json());
    }

    updateData(tableName: string, updatedDataContent: any, customHeaders?: any) {
        customHeaders = customHeaders || {};

        let headerObject = {};

        Object.assign(headerObject ,this.defaultHeaders, customHeaders);
        var headers = new Headers(headerObject);

        let Url: string = [this._everlive_url, this._app_id, tableName].join('/');

        return this.http.put(Url, updatedDataContent, {
            headers: headers
        }).map((res: Response) => res.json());
    }

    deleteData(tableName: string, itemId?: string, customHeaders?: any) {
        let Url: string;

        customHeaders = customHeaders || {};

        let headerObject = {};

        Object.assign(headerObject ,this.defaultHeaders, customHeaders);
        var headers = new Headers(headerObject);

        if (itemId) {
            Url = [this._everlive_url, this._app_id, tableName, itemId].join('/');
        } else {
            Url = [this._everlive_url, this._app_id, tableName].join('/');
        }

        return this.http.delete(Url, {
            headers: headers
        }).map((res: Response) => res.json());
    }

    readFunction(functionName: string, queryParameters?: any, customHeaders?: any) {
        let URL: string = "";

        customHeaders = customHeaders || {};

        let headerObject = {};

        Object.assign(headerObject ,this.defaultHeaders, customHeaders);
        var headers = new Headers(headerObject);

        if (customHeaders) {
            headers = this.addHeaders(headers, customHeaders);
        }

        URL = [this._everlive_url, this._app_id, 'Functions', functionName].join('/');

        if (queryParameters) {
            URL = [URL, '?', this.createQueryString(queryParameters)].join('');
        }

        return this.http.get(URL, {
            headers: headers
        }).map((res: Response) => res.json());
    }

    postFunction(functionName: string, postContent: any, customHeaders?: any) {


        customHeaders = customHeaders || {};

        let headerObject = {};

        Object.assign(headerObject ,this.defaultHeaders, customHeaders);
        var headers = new Headers(headerObject);


        if (customHeaders) {
            headers = this.addHeaders(headers, customHeaders);
        }

        let Url: string = [this._everlive_url, this._app_id, 'Functions', functionName].join('/');

        return this.http.post(Url, postContent, {
            headers: headers
        }).map((res: Response) => res.json());
    }

    addHeaders(headersObject: Headers, extraHeaders: any) {
        for (let header in extraHeaders) {
            if (extraHeaders.hasOwnProperty(extraHeaders[header])) {
                headersObject.append(header, extraHeaders[header]);
            }
        }

        return headersObject;
    }

    createQueryString(queryParameters: any): string {
        return Object.keys(queryParameters).map(function (key) {
            return [key, queryParameters[key]].map(encodeURIComponent).join("=");
        }).join("&");
    }

}
