import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class TitleService {

    private _current_title: string = "home";

    public get currentTitle(): string {
        return this._current_title;
    }

    public set currentTitle(newTitle: string) {
        if (newTitle && this._current_title !== newTitle) {
            this._current_title = newTitle;
            this._title.next(newTitle);
        } else {
            this._title.error('Duplicate Title');
        }

    }

    private _title: Subject<string> = new Subject<string>()

    public title: Observable<string> = this._title.asObservable();

    public _navigation: Subject<NavBarNavigationButton[]> = new Subject<NavBarNavigationButton[]>();

    public navigation: Observable<NavBarNavigationButton[]> = this._navigation.asObservable();

    public _extraButton: Subject<NavBarNavigationButton[]> = new Subject<NavBarNavigationButton[]>();

    public extraButtons: Observable<NavBarNavigationButton[]> = this._extraButton.asObservable();

    setTitle(newTitle) {
        this.currentTitle = newTitle;
    }

    setNavigation(buttons: NavBarNavigationButton[] = []) {
        this._navigation.next(buttons);
    }

    setExtraButtons(buttons: NavBarNavigationButton[] = []) {
        this._extraButton.next(buttons);
    }

}

export interface NavBarNavigationButton {
    icon?: string,
    text?: string,
    onClick: () => any,
    start?: boolean,
    end?: boolean,
    isMenuButton: boolean
}