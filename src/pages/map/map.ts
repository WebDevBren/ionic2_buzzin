import { Component, Input, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { GoogleMapsEvent, GoogleMapsLatLng, GoogleMapsLatLngBounds, Geolocation } from 'ionic-native';

import { CollectionsPage } from '../collections/collections';

import { MapService } from '../../services/map-service';
import { LocationService } from '../../services/location-service';
import { WikitudeService } from '../../services/wikitude-service';
import { EverliveService } from '../../services/everlive-service';
import { UserService } from '../../services/user-service';

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {

  @Input() AR_TITLE: string = "Map View";

  @ViewChild('map') public mapCanvas: ElementRef;

  public googleMap: any;

  public PageTitle = "MapView";

  currentLocationMarker: any;;


  bounds: GoogleMapsLatLngBounds[] = [];
  circles: any[] = [];
  markers: any[] = [];

  experienceName: string = "";
  experienceUrl: string = "";

  showBeginHuntButton: boolean = false;
  showBeginExperience: boolean = false;

  lastLocation: any = { latitude: 0, longitude: 0 };
  locationWatcher: any;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public GeoService: LocationService,
    public wikitude: WikitudeService,
    public everlive: EverliveService,
    public user: UserService,
    public params: NavParams,
    public toastCtrl: ToastController
  ) {

    let experience = params.get('experience');


    this.PageTitle = experience.Name;
    this.experienceName = experience.experienceName;
    this.experienceUrl = params.get('experienceUrl');

    console.log('experience', experience);
    console.log(this.experienceUrl);

  }

  mapLoaded: any;
  wikitudeWorldExit: any;
  wikitudeWorldLoad: any;
  isInit: boolean = false;

  ionViewWillEnter() {

    this.platform.ready().then(() => {
      var ManchesterLatLng = new plugin.google.maps.LatLng(53.481022, -2.242676);
      var map = this.googleMap = window["plugin"].google.maps.Map.getMap(this.mapCanvas.nativeElement, {
        'backgroundColor': 'white',
        'camera': {
          'latLng': ManchesterLatLng,
          'zoom': 15
        }
      });


      map.on(GoogleMapsEvent.MAP_READY, (map) => { 
        if( this.platform.is('ios') ) {
          this.mapCanvas.nativeElement.style.padding.top = "20px";
        }
        console.log('Map init'); this.initMap(map) 
      }); 
    
    });



    this.wikitudeWorldLoad = this.wikitude.experienceLoad.subscribe(
      (url) => {
        console.log('loaded', url);
        if(this.experienceName === 'ZodiacHunt') {
          this.wikitude.AR.callJavaScript(`World.setUserId( "${this.user.details.Id}")`);
        }
      },
      (error) => {

        let errorMessage;
        if (error) {
          errorMessage = error.message ? error.message : error;
        } else {
          errorMessage = "Oops, someting went wrong while loading that";
        }

        this.wikitude.AR.close();
        let wikitudeErrorToast = this.toastCtrl.create({
          message: errorMessage,
          duration: 3000
        });

        wikitudeErrorToast.present();
      }
    )


    this.wikitudeWorldExit = this.wikitude.WorldExit.subscribe(() => {
      console.log('wikitude world exit');

      this.googleMap.clear();

      if (this.experienceName === 'ZodiacHunt') {
        this.ZodiacHunt(this.googleMap);
      } else if (this.experienceName === 'TerracottaWarriors') {
        this.TerracottaWarriors(this.googleMap);
      }
    });
  }

  initMap(map) {
    this.isInit = true;

    if (this.experienceName === 'ZodiacHunt') {
      this.ZodiacHunt(map);
    } else if (this.experienceName === 'TerracottaWarriors') {
      this.TerracottaWarriors(map);
    }

  }

  ionViewDidLeave() {
    console.log('Maps did Leave');



    if (this.currentLocationMarker) {
      this.currentLocationMarker.remove();
    }

    if (this.wikitudeWorldLoad) {
      this.wikitudeWorldLoad.unsubscribe();
    }

    if (this.wikitudeWorldExit) {
      this.wikitudeWorldExit.unsubscribe();
    }

    if (this.ZodiacHuntDataPoints) {
      this.ZodiacHuntDataPoints.unsubscribe();
    }

    if (this.isInit) {
      this.googleMap.remove();
      this.destroyBounds();
      this.destroyCircles();
      this.isInit = false;
    }

  }

  loadExperience() {
    this.wikitude.loadWorld(this.experienceUrl);
  }

  goToCollections() {
    this.navCtrl.push(CollectionsPage);
  }

  ZodiacHuntDataPoints: any;

  ZodiacHunt(map: any) {
    this.destroyBounds();
    this.destroyCircles();
    this.googleMap = map; //just incase.

    this.getCurrentLocation().then(([pos, currentLatLng]) => {
      if (this.isInBounds(currentLatLng)) {
        this.showBeginHuntButton = true;
      } else {
        this.showBeginHuntButton = false;
      }

      this.showCurrentLocation(map, currentLatLng);
    });

    this.locationWatcher = Geolocation.watchPosition().subscribe(
      (pos: Position) => {
        let newLatLng = new GoogleMapsLatLng(pos.coords.latitude, pos.coords.longitude);
        this.showCurrentLocation(map, newLatLng);

        if (this.isInBounds(newLatLng)) {
          this.showBeginHuntButton = true;
        } else {
          this.showBeginHuntButton = false;
        }

      }
    )

    this.ZodiacHuntDataPoints = this.everlive
      .readFunction('getZodiacs', { 'userId': this.user.details.Id })
      .subscribe(
      (data) => {

        data.result.forEach((item) => {

          if (item.HasBeenCollected) {

            var latLng = new GoogleMapsLatLng(item.Location.latitude, item.Location.longitude);

            map.addCircle({
              center: latLng,
              radius: 25,
              strokeColor: '#DBAB3E',
              fillColor: '#DBAB3E',
              strokeWidth: 4
            }, (circle) => this.circles.push(circle));

            var rawPoints = this.getLatLngByRadius(item.Location, 25);

            var convertedPoints = [
              new GoogleMapsLatLng(rawPoints.North.latitude, rawPoints.North.longitude),
              new GoogleMapsLatLng(rawPoints.NorthWest.latitude, rawPoints.NorthWest.longitude),
              new GoogleMapsLatLng(rawPoints.NorthEast.latitude, rawPoints.NorthEast.longitude),
              new GoogleMapsLatLng(rawPoints.South.latitude, rawPoints.South.longitude),
              new GoogleMapsLatLng(rawPoints.SouthEast.latitude, rawPoints.SouthEast.longitude),
              new GoogleMapsLatLng(rawPoints.SouthWest.latitude, rawPoints.SouthWest.longitude),
              new GoogleMapsLatLng(rawPoints.East.latitude, rawPoints.East.longitude),
              new GoogleMapsLatLng(rawPoints.West.latitude, rawPoints.West.longitude),
              // new plugin.google.maps.LatLng(rawPoints.Center.latitude, rawPoints.Center.longitude),
            ];

            map.addMarker({
              'title': ` This is the ${item.Name} Zodiac, You've already found this one! `,
              'position': new GoogleMapsLatLng(rawPoints.Center.latitude, rawPoints.Center.longitude)
            }, (marker) => this.markers.push(marker));

            let latlngBounds = new GoogleMapsLatLngBounds(convertedPoints);

            this.bounds.push(latlngBounds);

          } else {

            var latLng = new GoogleMapsLatLng(item.Location.latitude, item.Location.longitude);

            map.addCircle({
              center: latLng,
              radius: 25,
              strokeColor: '#DBAB3E',
              fillColor: '#E20613',
              strokeWidth: 4
            }, (circle) => this.circles.push(circle));

            var rawPoints = this.getLatLngByRadius(item.Location, 25);

            var convertedPoints = [
              new GoogleMapsLatLng(rawPoints.North.latitude, rawPoints.North.longitude),
              new GoogleMapsLatLng(rawPoints.NorthWest.latitude, rawPoints.NorthWest.longitude),
              new GoogleMapsLatLng(rawPoints.NorthEast.latitude, rawPoints.NorthEast.longitude),
              new GoogleMapsLatLng(rawPoints.South.latitude, rawPoints.South.longitude),
              new GoogleMapsLatLng(rawPoints.SouthEast.latitude, rawPoints.SouthEast.longitude),
              new GoogleMapsLatLng(rawPoints.SouthWest.latitude, rawPoints.SouthWest.longitude),
              new GoogleMapsLatLng(rawPoints.East.latitude, rawPoints.East.longitude),
              new GoogleMapsLatLng(rawPoints.West.latitude, rawPoints.West.longitude),
              // new plugin.google.maps.LatLng(rawPoints.Center.latitude, rawPoints.Center.longitude),
            ];

            map.addMarker({
              'title': 'Mystery Animal, Come over to try and find it!',
              'position': new GoogleMapsLatLng(rawPoints.Center.latitude, rawPoints.Center.longitude)
            }, (marker) => this.markers.push(marker));

            let latlngBounds = new GoogleMapsLatLngBounds(convertedPoints);

            this.bounds.push(latlngBounds);
          }

        });
      },
      (error) => {
        console.log(error)
      });
  }

  TerracottaWarriors(map: any) {
    this.destroyBounds();
    this.destroyCircles();
    this.googleMap = map; //just incase.

    var tcotta_location = { latitude: 53.484387, longitude: -2.243086 };
    var tcotta = new GoogleMapsLatLng(tcotta_location.latitude, tcotta_location.longitude);

    map.addCircle({
      center: tcotta,
      radius: 100,
      strokeColor: '#6E1B69',
      strokeWidth: 4,
      fillColor: '#6E1B69'
    }, (circle) => {

      map.addMarker({
        'title': 'Terracotta warriors Come alive!',
        'position': tcotta
      }, (marker) => this.markers.push(marker));

      var rawPoints = this.getLatLngByRadius(tcotta_location, 100);

      var convertedPoints = [
        new GoogleMapsLatLng(rawPoints.North.latitude, rawPoints.North.longitude),
        new GoogleMapsLatLng(rawPoints.NorthWest.latitude, rawPoints.NorthWest.longitude),
        new GoogleMapsLatLng(rawPoints.NorthEast.latitude, rawPoints.NorthEast.longitude),
        new GoogleMapsLatLng(rawPoints.South.latitude, rawPoints.South.longitude),
        new GoogleMapsLatLng(rawPoints.SouthEast.latitude, rawPoints.SouthEast.longitude),
        new GoogleMapsLatLng(rawPoints.SouthWest.latitude, rawPoints.SouthWest.longitude),
        new GoogleMapsLatLng(rawPoints.East.latitude, rawPoints.East.longitude),
        new GoogleMapsLatLng(rawPoints.West.latitude, rawPoints.West.longitude),
        // new plugin.google.maps.LatLng(rawPoints.Center.latitude, rawPoints.Center.longitude),
      ];

      var latlngBounds = new GoogleMapsLatLngBounds(convertedPoints);
      this.bounds.push(latlngBounds);
    });

    Geolocation.watchPosition().subscribe(
      (pos: Position) => {
        let newLatLng = new GoogleMapsLatLng(pos.coords.latitude, pos.coords.longitude);
        if( this.currentLocationMarker ) {
          this.showCurrentLocation(map, newLatLng);
        }
        if (this.isInBounds(newLatLng)) {
          this.showBeginHuntButton = true;
        } else {
          this.showBeginHuntButton = false;
        }


      }
    )

    this.getCurrentLocation().then(([pos, latlng]) => {
      this.createCurrentLocationMarker(map, latlng, 'Go to Exchange Square to experience the Terracotta warriors come to life');
      
    });

  }

  showCurrentLocation(Map: any, Position: GoogleMapsLatLng) {
    if (this.currentLocationMarker) {
      this.currentLocationMarker.setPosition(Position);
    } else {
      this.createCurrentLocationMarker(Map, Position);
    }
  }

  isInBounds(CurrentPosition: GoogleMapsLatLng) {
    console.log('checking if in bounds');
    return this.bounds.find((bounds) => bounds.contains(CurrentPosition)) ? true : false;
  }

  createCurrentLocationMarker(map: any, Position: GoogleMapsLatLng, msg? : string) {

    if (!this.currentLocationMarker) {


      var ICON_SIZE = {
        width: 120,
        height: 131,
        scale: 0.35
      };


      map.addMarker({
        title: 'It\'s you!',
        snippet: msg ? msg :'Travel to the Red Circles to Find the Hidden Zodiac\'s ',
        position: Position,
        icon: {
          url: 'https://bs3.cdn.telerik.com/v1/lacs5sag3ylbwzm7/ed1c19a0-dd76-11e6-9e95-4951680b9ca7',
          size: {
            width: Math.round(ICON_SIZE.width * ICON_SIZE.scale),
            height: Math.round(ICON_SIZE.height * ICON_SIZE.scale)
          }
        }
      }, (marker) => {
        marker.showInfoWindow();

        this.currentLocationMarker = marker;

      });

    } else {
      this.currentLocationMarker.setPosition(Position);
    }

  }

  getCurrentLocation(): Promise<any> {
    return new Promise((res, rej) => {

      Geolocation.getCurrentPosition().then((pos) => {
        console.log('got location', pos);
        let currentLatLng = new GoogleMapsLatLng(pos.coords.latitude, pos.coords.longitude);

        this.googleMap.setCenter(currentLatLng);
        this.googleMap.setZoom(16);

        res([res, currentLatLng]);
      },
        (err: PositionError) => { console.warn(err.message); rej(err) }
      );

    })
  }

  destroyBounds() {
    this.bounds = [];
  }

  destroyCircles() {
    this.circles.forEach(circle => circle.remove());
  }

  getLatLngByRadius(location, radius) {
    var calculate = function (offsetX, offsetY) {
      offsetY = offsetY || 0;
      offsetX = offsetX || 0;

      var R = 6378137;

      var dn = offsetX;
      var de = offsetY;

      var dLat = dn / R;
      var dLon = de / (R * Math.cos(Math.PI * location.latitude / 180));

      var lat, lng;

      lat = location.latitude + dLat * 180 / Math.PI;
      lng = location.longitude + dLon * 180 / Math.PI;
      return {
        latitude: lat,
        longitude: lng
      };
    }
    return {
      Center: location,
      North: calculate(radius, null),
      NorthWest: calculate(radius, -radius),
      NorthEast: calculate(radius, radius),
      South: calculate(-radius, null),
      SouthWest: calculate(-radius, -radius),
      SouthEast: calculate(-radius, radius),
      East: calculate(0, radius),
      West: calculate(0, -radius)
    };
  }

}
