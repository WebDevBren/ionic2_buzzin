import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { NavController, ToastController, Toast } from 'ionic-angular';
import { UserService } from '../../services/user-service';

import { matchingPasswords, emailValidator } from '../../validators/validators';

@Component({
    selector: 'page-registration',
    templateUrl: './registration.html',

})
export class RegistrationPage {

    emailRegex: '^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$';

    successToast: Toast;
    failureToast: Toast;


    navigationItems: any[] = [
        {
            icon: 'md-arrow-back',
            onClick: () => { this.navCtrl.push(this.navCtrl.getPrevious()) },
            isMenuButton: false
        }
    ];

    registrationForm: FormGroup = this.formBuilder.group({
        username: [null, Validators.required],
        email: [null, Validators.compose([Validators.required, emailValidator])],
        displayname: [null, Validators.required],
        password: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(16)])],
        password_confirm: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(16)])]
    }, { validator: matchingPasswords('password', 'password_confirm') });

    constructor(private formBuilder: FormBuilder, public navCtrl: NavController, public toastCtrl: ToastController, public user: UserService) {
        this.successToast = this.toastCtrl.create({
            message: 'Successfully Registered, Please check your email to validate your account. Welcome to Buzzin.',
            duration: 3000
        });

        this.failureToast = this.toastCtrl.create({
            message: 'Failed to create account, please check your internet connection and try again.',
             duration: 3000
        });
    }


    ionViewWillEnter() {

    }


    submit() {
        this.user.register(
            this.registrationForm.controls['username'].value,
            this.registrationForm.controls['password'].value,
            this.registrationForm.controls['email'].value,
            this.registrationForm.controls['displayname'].value,
        ).subscribe(
            () => { this.successToast.present(); },
            () => { this.failureToast.present(); }
            );
    }

}