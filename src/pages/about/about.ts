import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { GoogleMap } from 'ionic-native';

import { MapService } from '../../services/map-service';


//ModalController

import { LicensePage } from '../license/license';

/*
  Generated class for the About page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {


  constructor(public navCtrl: NavController, public navParams: NavParams, public mapsCtrl: MapService, public modalCtrl: ModalController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

  showMapsLicense() {

    if( this.mapsCtrl.isInit ) {  
    this.mapsCtrl.map.getLicenseInfo().then((text) => {
      let MapsLicenseModal = this.modalCtrl.create(LicensePage,{
        license: text
      });

      MapsLicenseModal.present();
    })
    }

    
  }

}
