import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

/*
  Generated class for the License page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-license',
  templateUrl: 'license.html'
})
export class LicensePage {

  licenseText: string  = '';

  constructor(params: NavParams, public viewCtrl: ViewController) {
    this.licenseText = params.get('license');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LicensePage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
