import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { NavController, ToastController, Toast } from 'ionic-angular';
import { UserService } from '../../services/user-service';

@Component({
    selector: 'page-reset-password',
    templateUrl: './reset-password.html',

})
export class ResetPasswordPage {

    emailRegex: '^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$';

    resetPassword: FormGroup = this.formBuilder.group({
        email: ['', Validators.compose([Validators.required, Validators.pattern(this.emailRegex)])]
    });

    navigationItems: any[] = [
        {
            icon: 'md-arrow-back',
            onClick: () => { this.navCtrl.push(this.navCtrl.getPrevious()) },
            isMenuButton: false
        }
    ];

    successToast: Toast;
    failedToast: Toast;

    constructor(private formBuilder: FormBuilder, public navCtrl: NavController, public toastCtrl: ToastController, public user: UserService) {
        this.successToast = this.toastCtrl.create({
            message: 'Success! Please check your email to finish resetting your password.',
            duration: 3000
        });

        this.failedToast = this.toastCtrl.create({
            message: 'Oops! Something went wrong!',
            duration: 3000
        });
    }



    ionViewWillEnter() {

    }



    submit() {
        this.user
            .resetPassword(this.resetPassword.controls['email'].value)
            .subscribe(
            () => {
                this.successToast.present();
                this.navCtrl.last();
            },
            () => { this.failedToast.present(); }
            )
    }

}