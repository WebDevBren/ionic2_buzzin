import { Component } from '@angular/core';
import { ToastController, Toast, ModalController, Modal, NavController } from 'ionic-angular';
import { UserService } from '../../services/user-service';
import { CollectionItemInfo } from '../../app/CollectionItem';

import { PrizeRedemptionPage } from '../prize-redemption/prize-redemption';

@Component({
    selector: 'page-collections',
    templateUrl: './collections.html'
})
export class CollectionsPage {

    errorToast: Toast = null;

    collections: any[] = [];

    constructor(public user: UserService, public toastCtrl: ToastController, public modalCtrl: ModalController, public navCtrl: NavController) {
        this.errorToast = this.toastCtrl.create({
            message: 'An Error Occured while getting your Collections, Please Try again later',
            duration: 3000
        });

    };

    ionViewDidEnter() {
        this.getContents();
    }

    moreInfo($event) {
        console.log($event);
        let moreInfoModal = this.modalCtrl.create(CollectionItemInfo, { title: $event.value.Name, description: $event.value.Info });
        moreInfoModal.present();
    }

    getContents() {
        this.user
            .everlive
            .readFunction('GetCollections', { userId: this.user.details.Id })
            .subscribe(
            (data) => {
                data.forEach(item => this.collections.push(item));
            },
            (error) => { this.errorToast.present() }
            );
    }


    onPrizeButtonClick($event) {
        let Collection = $event.value;
        this.user.everlive.postFunction('RedeemCollectionPrize', {
            userId: this.user.details.Id,
            collectionId: Collection.Id
        }).subscribe(
            (res) => {

                let data = res.json();

                this.navCtrl.push(PrizeRedemptionPage, {
                    HeaderImage: Collection.PrizeHeader,
                    Description: Collection.PrizeDescription,
                    UniqueCode: data.result.UniqueCode,
                    CreatedDate: new Date(data.result.CreatedAt)
                });
            }
        );
    }

}