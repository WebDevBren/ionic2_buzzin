import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { SocialSharing } from 'ionic-native';

/*
  Generated class for the PrizeRedemption page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-prize-redemption',
  templateUrl: 'prize-redemption.html'
})
export class PrizeRedemptionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {
    this.data.HeaderImage = navParams.get('HeaderImage');
    this.data.Description = navParams.get('Description');
    this.data.UniqueCode = navParams.get('UniqueCode');
    this.data.CreatedAt = navParams.get('CreatedDate');
  }

  data: any = {};

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrizeRedemptionPage');
  }

  shareOptions: {
    message: 'Wow! I just won a prize playing the Buzzin App Zodaic Hunt!', // not supported on some apps (Facebook, Instagram)
    subject: 'Come Check this App Out!', // fi. for email
    files: ['https://bs1.cdn.telerik.com/v1/lacs5sag3ylbwzm7/70b619c0-dcc1-11e6-995d-332dca886846'], // an array of filenames either locally or remotely
    url: 'http://buzzinapp.com/',
    chooserTitle: 'Pick an app' // Android only, you can override the default share sheet title
  };

  shareNow() {
    SocialSharing.shareWithOptions(this.shareOptions).then(shareSuccess => shareSuccess ? this.onShareSuccess() : this.onShareFail());
  }

  onShareSuccess() {
    let successToast = this.toastCtrl.create({
      message: 'Success!'
    });

    successToast.present();
  }

  onShareFail() {
    let failtoast = this.toastCtrl.create({
      message: 'Failed to Share!',
      showCloseButton: true,
      closeButtonText: 'Try Again'
    });

    failtoast.present();
  }

}
