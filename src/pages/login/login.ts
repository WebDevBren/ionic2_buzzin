import { Component } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { UserService } from '../../services/user-service';
import { NavController } from 'ionic-angular';
import { Facebook } from 'ionic-native';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { emailValidator } from '../../validators/validators';

//pages
import { HomePage } from '../home/home';
import { ResetPasswordPage } from '../resetPassword/reset-password';
import { RegistrationPage } from '../registration/registration';

@Component({
    'selector': 'page-login',
    templateUrl: './login.html',

})
export class LoginPage {

    rememberMe: boolean = false;

    constructor(public navCtrl: NavController, public toastCtrl: ToastController, public fb: FormBuilder, public user: UserService) {

    }

    public loginForm: FormGroup = this.fb.group({
        'email': [null, Validators.compose([Validators.required, emailValidator])],
        'password': [null, Validators.required]
    });

    onCheckbox($event) {
        console.log($event);
        this.rememberMe = $event.checked;
    }

    ionViewWillEnter() {

        this.user.AuthState.subscribe((AuthState) => {
            if( AuthState.state === this.user.states.LOGGED_IN ) {
                this.loginSuccess();
            } else if( AuthState.state === this.user.states.LOGGED_OUT ) {
                this.logOutSuccess();
            } else if ( AuthState.state === this.user.states.ERROR ) {
                this.loginFail();
            }
        }, (error) => {
            console.log( error.detail );
            this.loginFail();
        });

        // this.user.loggedIn.subscribe(() => { this.loginSuccess() }, () => { this.loginFail() });
    }



    submit() {
        console.log('remember me ?', this.rememberMe);
        this.user.login(this.loginForm.controls['email'].value, this.loginForm.controls['password'].value, this.rememberMe);
    }

    logOutSuccess() {
        var toast = this.toastCtrl.create({
            message: 'Successfully logged Out!',
            duration: 3000
        });

        toast.present();

        //navigate back to home?
        this.navCtrl.setRoot(HomePage);
    }

    loginSuccess() {
        var toast = this.toastCtrl.create({
            message: 'Successfully logged In!',
            duration: 3000
        });

        toast.present();

        //navigate back to home?
        this.navCtrl.setRoot(HomePage);
    }

    loginFail() {
        var toast = this.toastCtrl.create({
            message: 'Failed! Please check your details and try again.',
            duration: 3000
        });

        toast.present();
    }

    goToResetPassword() {
        this.navCtrl.push(ResetPasswordPage);
    }

    goToRegistration() {
        this.navCtrl.push(RegistrationPage);
    }

    facebookLogin() {
        Facebook.login(['email', 'public_profile']).then((res) => {
            this.user.loginOrRegisterWithProvider('Facebook', res.authResponse.accessToken);
        }, (err) => {
            console.log(err);
            var toast = this.toastCtrl.create({
            message: 'Oops! Something went wrong, please try again.',
            duration: 3000
        });

        toast.present();
        });
    }

}