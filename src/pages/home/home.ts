import { Component } from '@angular/core';

import { NavController, Platform, ToastController } from 'ionic-angular';
import { MapPage } from '../map/map';
import { LoginPage } from '../login/login';

import { Diagnostic } from 'ionic-native';

import { LocationService } from '../../services/location-service'
import { WikitudeService } from '../../services/wikitude-service';
import { UserService } from '../../services/user-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  public ARItems = [
    // {
    //   Name: "Zodiac Hunt!",
    //   experienceName: 'ZodiacHunt',
    //   Description: 'Hunt around Manchester looking for the Zodiacs, Unlock 3 or more and get a mystery prize',
    //   ImageUrl: 'assets/img/ZodiacHunt.png',
    //   requiresAuth: true
    // },
    {
      Name: "Terracotta Warriors",
      experienceName: 'TerracottaWarriors',
      Description: 'Go to Exhange Square and bring the Terracotta Warriors alive!',
      ImageUrl: 'assets/img/Terracotta.png',
    }
  ];

  public location: any = { latitude: 0, longitude: 0 };

  public wikitude: WikitudeService;

  public wikitudeWorldLoad: any;

  public wikitudeSupported: boolean = false;

  constructor(public navCtrl: NavController, public platform: Platform, public geoservice: LocationService, public user: UserService, public ToastCtrl: ToastController) {

    platform.ready().then(() => {

      if (this.platform.is('cordova')) {

        this.wikitude = new WikitudeService();

        this.wikitude.deviceSupport.subscribe(() => {
          console.log('device is supported');
          this.wikitudeSupported = true;
        }, () => {
          this.wikitudeSupported = false;
        });

        this.wikitude.checkDeviceCompatability();

        this.wikitudeWorldLoad = this.wikitude.experienceLoad.subscribe(
          (url) => {
            console.log('loaded', url);

          },
          (error) => {

            let errorMessage;
            if (error) {
              errorMessage = error.message ? error.message : error;
            } else {
              errorMessage = "Oops, someting went wrong while loading that";
            }

            this.wikitude.AR.close();
            let wikitudeErrorToast = this.ToastCtrl.create({
              message: errorMessage,
              duration: 3000
            });

            wikitudeErrorToast.present();
          }
        )

      }

    });


  }

  goToLogin() {
    this.navCtrl.push(LoginPage);
  }

  ionViewDidEnter() {

    if (this.platform.is('cordova')) {

      Diagnostic.isLocationAuthorized().then((authorised) => {
        console.log(authorised);
        if (!authorised) {
          Diagnostic.requestLocationAuthorization();
        }
      });

      Diagnostic.isCameraAuthorized().then((authorised) => {
        console.log(authorised);
        if (!authorised) {
          Diagnostic.requestCameraAuthorization();
        }
      });
    }


  }

  openExperience(item, requiresAuth: boolean = false) {

    var AR_EXPERIENCES = {
      TerracottaWarriors: 'https://testproject-30a64.firebaseapp.com/AR_WORLDS/TerracottaWarriors/',
      ZodiacHunt: 'https://testproject-30a64.firebaseapp.com/AR_WORLDS/ZodiacHunt/',
      ScanWorld: 'https://testproject-30a64.firebaseapp.com/AR_WORLDS/ScanWorld/'
    }

    // if (requiresAuth) {
    //   if (this.user.isLoggedIn && this.user.details.Id) {
    //     this.navCtrl.push(MapPage, {
    //       experience: item,
    //       experienceUrl: AR_EXPERIENCES[item.experienceName]
    //     });
    //   } else {
    //     this.navCtrl.setRoot(LoginPage);
    //   }
    // } else {
    //   this.navCtrl.push(MapPage, {
    //     experienceName: item,
    //     experienceUrl: AR_EXPERIENCES[item.experienceName]
    //   });
    // }
    this.wikitude.loadWorld(AR_EXPERIENCES.TerracottaWarriors);


    }

    directOpen() {
      this.wikitude.loadWorld('https://testproject-30a64.firebaseapp.com/AR_WORLDS/ScanWorld/');
    }

  }
